"""
Instrucciones de ejecución:
python3 ejercicio2.py
"""
__author__ = "André Michel Pozos Nieto"


def proceso_match(a1, a2):
    """
    Este es el proceso match
    """
    i = 0
    j = 0
    final = []

    while(i < len(a1) and j < len(a2)):
        if(a1[i] == a2[j]):
            final.append(a1[i])
            final.append(a2[j])
            i += 1
            j += 1
        elif(a1[i] > a2[j]):
            final.append(a2[j])
            j += 1
        else:
            final.append(a1[i])
            i += 1

    while(i < len(a1)):
        final.append(a1[i])
        i += 1
    while(j < len(a2)):
        final.append(a2)
        j += 1

    return final


if __name__ == "__main__":
    a1 = [1, 2, 6, 8, 9]
    a2 = [3, 5, 6, 9]
    final = proceso_match(a1, a2)
    print(final)
