"""
Instrucciones de ejecución:
python3 ejercicio1.py
"""
__author__ = "André Michel Pozos Nieto"


def numero_mas_grande(numeros):
    """
    Encuentra el número más grande dentro de una lista.
    """

    pos = 0
    mayor = float("-inf")
    for (i, num) in enumerate(numeros):
        if(num > mayor):
            mayor = num
            pos = i
    print(f"El valor máximo es {mayor} y está en {pos}")


if __name__ == "__main__":
    numero_mas_grande([-5, 6, 0, -1, -22])
