import csv
import re

"""
Instrucciones de ejecución:
python3 ejercicio3.py
"""
__author__ = "André Michel Pozos Nieto"


def extraer_int(texto):
    """
    Extrae la cantidad del CSV y la convierte a entero.
    """
    return int(re.search(r"\d+,\d+", texto).group(0).replace(",", ""))


def mostrar_reporte(resultados):
    """
    Imprime los resultados en el formato de reporte solicitado
    """
    total_p = 0
    total_imp = 0
    for (sucursal, categorias) in resultados.items():
        piezas_totales = 0
        importe_total = 0
        for (categoria, valores) in categorias.items():
            piezas = valores["piezas"]
            piezas_totales += piezas

            importe = valores["importe"]
            importe_total += importe

            precio_prom = importe / piezas

            print(f"{sucursal} - {categoria} - {piezas} - {importe} - {precio_prom}")
        promedio_sucursal = importe_total/piezas_totales
        total_p += piezas_totales
        total_imp += importe_total
        print(f"{sucursal} - {piezas_totales} - {importe_total} - {promedio_sucursal}")
    total_prom = total_imp / total_p
    print(f"Total - {total_p} - {total_imp} - {total_prom}")


def generar_reporte():
    """
    Lee el archivo csv y convierte la información en un diccionario.
    """
    with open("reporte.csv", "r") as reporte:
        csv_reader = csv.reader(reporte)

        # Con esto nos saltamos las 2 primeras líneas
        next(csv_reader)
        next(csv_reader)

        resultados = {}
        # Aquí ya tenemos los datos listos
        for linea in csv_reader:
            tienda = linea[0]
            categoria = linea[1]

            if(not tienda in resultados.keys()):
                resultados[tienda] = {}
            if(not categoria in resultados[tienda].keys()):
                resultados[tienda][categoria] = {
                    "piezas": 0,
                    "importe": 0
                }

            resultados[tienda][categoria]["piezas"] += int(linea[3])
            resultados[tienda][categoria]["importe"] += extraer_int(linea[6])

    mostrar_reporte(resultados)


if __name__ == "__main__":
    generar_reporte()
