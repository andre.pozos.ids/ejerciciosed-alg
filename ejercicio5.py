"""
Instrucciones de ejecución:
python3 ejercicio5.py
"""
__author__ = "André Michel Pozos Nieto"


def heapify(numeros, tamaño, i):
    """
    Crea el heap y hace el swap de la raíz con el elemento inferior del heap.
    """
    largest = i
    left = 2*i + 1
    right = 2*i + 2
    if(left < tamaño and numeros[left] > numeros[i]):
        largest = left
    if(right < tamaño and numeros[right] > numeros[largest]):
        largest = right
    if(largest != i):
        numeros[i], numeros[largest] = numeros[largest], numeros[i]
        heapify(numeros, tamaño, largest)


def heap_sort(numeros):
    """
    Hace el proceso de heap sort.
    """
    tamaño = len(numeros)
    for i in range(tamaño // 2 - 1, -1, -1):
        heapify(numeros, tamaño, i)
    for j in range(tamaño - 1, 0, -1):
        numeros[j], numeros[0] = numeros[0], numeros[j]
        heapify(numeros, j, 0)


if __name__ == "__main__":
    numeros = [59, -88, -65, -45, 80]
    print(numeros)
    heap_sort(numeros)
    print(numeros)
