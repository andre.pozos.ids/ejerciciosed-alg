"""
Instrucciones de ejecución:
python3 ejercicio4.py
"""
__author__ = "André Michel Pozos Nieto"


def merge(numeros, izq, mitad, der):
    """
    Hace el proceso de unir los 2 arreglos.
    """
    tam_izq = mitad - izq + 1
    tam_der = der - mitad

    mitad_izq = [0] * tam_izq
    mitad_der = [0] * tam_der
    for i in range(0, tam_izq):
        mitad_izq[i] = numeros[izq + i]

    for j in range(0, tam_der):
        mitad_der[j] = numeros[mitad + j + 1]

    i = 0
    j = 0
    k = izq

    while(i < tam_izq and j < tam_der):
        if(mitad_izq[i] <= mitad_der[j]):
            numeros[k] = mitad_izq[i]
            i += 1
        else:
            numeros[k] = mitad_der[j]
            j += 1
        k += 1

    while(i < tam_izq):
        numeros[k] = mitad_izq[i]
        i += 1
        k += 1

    while(j < tam_der):
        numeros[k] = mitad_der[j]
        j += 1
        k += 1


def merge_sort(numeros, izq, der):
    """
    Separa los arreglos a la mitad recursivamente.
    """
    if(izq < der):
        mitad = (izq + der) // 2
        merge_sort(numeros, izq, mitad)
        merge_sort(numeros, mitad + 1, der)
        merge(numeros, izq, mitad, der)


if __name__ == "__main__":
    numeros = [59, -88, -65, -45, 80, -96, 65, 27, -99, 7]
    print(numeros)
    merge_sort(numeros, 0, len(numeros) - 1)
    print(numeros)
